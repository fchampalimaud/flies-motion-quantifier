import cv2, numpy as np
from scipy.ndimage.filters import gaussian_filter
from math import factorial

def lin_dist( p1, p2 ):   return np.linalg.norm( (p1[0]-p2[0], p1[1]-p2[1]) )

def biggestContour(contours, howmany=1):
	biggest = []
	for blob in contours:
		area = cv2.contourArea(blob)
		biggest.append( (area, blob) )
	if len(biggest)==0: return None
	biggest = sorted( biggest, key=lambda x: -x[0])
	if howmany==1: return biggest[0][1]
	return [x[1] for x in biggest[:howmany] ]

def getBiggestContour(image, howmany=1):
	(_, blobs, dummy) = cv2.findContours( image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	return biggestContour(blobs, howmany)






def smooth(y, box_pts):
	box = np.ones(box_pts)/box_pts
	y_smooth = np.convolve(y, box, mode='same')
	return y_smooth

def savitzky_golay(y, window_size, order=0, deriv=0, rate=1):
	y = np.array(y)
	try:
	    window_size = np.abs(np.int(window_size))
	    order = np.abs(np.int(order))
	except(ValueError, msg):
	    raise ValueError("window_size and order have to be of type int")
	if window_size % 2 != 1 or window_size < 1:
	    raise TypeError("window_size size must be a positive odd number")
	if window_size < order + 2:
	    raise TypeError("window_size is too small for the polynomials order")
	order_range = range(order+1)
	half_window = (window_size -1) // 2
	# precompute coefficients
	b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
	m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
	# pad the signal at the extremes with
	# values taken from the signal itself
	firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
	lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
	y = np.concatenate((firstvals, y, lastvals))
	return np.convolve( m[::-1], y, mode='valid')


