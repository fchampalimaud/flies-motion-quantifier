import sys, os, shutil, re, pyforms, numpy as np, cv2
from pyforms 			 import BaseWidget
from pyforms.Controls 	 import ControlFile
from pyforms.Controls 	 import ControlPlayer
from pyforms.Controls 	 import ControlButton
from pyforms.Controls 	 import ControlNumber
from pyforms.Controls 	 import ControlSlider
from pyforms.Controls 	 import ControlCheckBox
from pyforms.Controls 	 import ControlText

from flies_motion_quantifier.motion_counter import MotionCounter
from PyQt5 import QtGui

if sys.platform.startswith('linux') or sys.platform.startswith('cygwin'): os.system('taskset -p 0xff %d' % os.getpid())


class Main(BaseWidget, MotionCounter):

	def __init__(self):
		MotionCounter.__init__(self)
		BaseWidget.__init__(self, 'Fly motion')

		self._video_file 		= ControlFile('Video file')
		self._idtracker_file 	= ControlFile('Flies positions file')
		self._player			= ControlPlayer('Player')
		self._no_flies			= ControlNumber('No. flies', default=1)
		self._load_btn 			= ControlButton('Load')
		self._show_diff			= ControlCheckBox('Show diffs boxes')
		

		self._led_roi			= ControlText('Led ROI')
		self._threshold_slider	= ControlSlider('Threshold', default=5, minimum=1, maximum=255)
		self._radius_slider		= ControlSlider('Radius', default=30, minimum=1, maximum=200)
		
		self._formset = [
			'_video_file', 
			('_idtracker_file','_no_flies','_load_btn'),
			('_led_roi', '_threshold_slider', '_radius_slider', '_show_diff'),
			'_player'
		]

		self._player.process_frame_event 	= self.__process_frame
		self._load_btn.value				= self.__load_data_event
		self._threshold_slider.changed_event= self.__threshold_changed_event
		self._radius_slider.changed_event 	= self.__radius_changed_event
		self._video_file.changed_event    	= self.__video_file_changed_event


		#self._video_file.value 		= '/home/ricardo/subversion/social-modulation-of-defence-behaviours-in-drosophila/data/Subjectb0/subjectb0.avi'
		#self._idtracker_file.value 	= '/home/ricardo/subversion/social-modulation-of-defence-behaviours-in-drosophila/data/Subjectb0/trajectories_nogaps.txt'


	def __video_file_changed_event(self): 
		self._player.value  = self._video_file.value
		self._led_roi.value	= "{0},{1},{2}".format(50, int(self._player.frame_height-50) ,50)

	def __load_data_event(self):
		self.load_data(self._idtracker_file.value, int(self._no_flies.value))
		self.message("The file was loaded with success.","Data loaded")

	def __process_frame(self, frame):
		index = self._player.video_index

		for fly in self.flies: fly.process(index, frame)
		for fly in self.flies: fly.draw(index, frame, self._show_diff.value)

		try:
			x,y,radius = eval(self._led_roi.value)
			cv2.circle(frame, (x,y), radius, (255,0,255), 1)
		except:
			pass

		return frame


	def __threshold_changed_event(self): 
		self.threshold = self._threshold_slider.value
		for fly in self.flies: fly.threshold = self.threshold
	def __radius_changed_event(self): 	 self.radius = self._radius_slider.value



	def execute(self):
		if not os.path.exists('output'): os.makedirs('output')

		self.led = eval(self._led_roi.value)
		
		cap = cv2.VideoCapture(self._video_file.value)
		self.load_data(self._idtracker_file.value, self._no_flies.value)

		self.start_progress( int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) )

		while True:
			res, frame = cap.read()
			if not res or self.stop: break

			index = int(cap.get(cv2.CAP_PROP_POS_FRAMES))

			for fly in self.flies: fly.process(index, frame)
			for fly in self.flies: fly.check_collisions(index, self.flies)
			self.process(frame)

			self.update_progress()

		for i, fly in enumerate(self.flies): 
			fly.export_2_csv('output', i, self._led_light, total_n_flies=len(self.flies), data2save=[ 'Threshold value', self._threshold_slider.value, 'Radius', self._radius_slider.value] )
		self.export_2_csv('output')

		self.end_progress()


if __name__ == "__main__": pyforms.start_app(Main)