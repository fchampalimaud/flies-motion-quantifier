import csv, cv2, os, numpy as np
from flies_motion_quantifier import tools
from scipy.ndimage.filters import gaussian_filter

class Fly(object):

	def __init__(self, name):
		self._name 			= str(name)
		self._positions 	= []
		self._velocities	= []
		self._accels 		= []
		self._motion 		= []
		self._absmotion 	= []
		self._events 		= []
		self._open_events 	= []
		self._last_img  	= None
		self._last_diff 	= None
		self._mask			= None
		self._collisions 	= []

		self.threshold 	= 5
		self.radius 	= 30


	def addPosition(self, x, y):
		try:
			pos = (int(round(float(x))),int(round(float(y))) )
		except:
			if len(self._positions)>0: 
				pos = self._positions[-1]
			else: 
				pos = self._radius,self._radius
		self._velocities.append( 0 if len(self._positions)==0 else tools.lin_dist(self._positions[-1], pos) 	)
		self._accels.append( 	 0 if len(self._velocities)<=1 else self._velocities[-1]-self._velocities[-2] 	)

		self._positions.append( pos )

	def init(self):
		self._last_img  	= None
		self._last_diff 	= None
		

	def process(self, index, frame):
		if index>=len(self._positions): return
		x,y		= self._positions[index]
		cutx 	= int(round(x-self._radius))
		cuty 	= int(round(y-self._radius))
		cutxx 	= int(round(x+self._radius))
		cutyy 	= int(round(y+self._radius))
		if cutx<0: cutx=0; cutxx=self._radius*2
		if cuty<0: cuty=0; cutyy=self._radius*2

		small	= frame[cuty:cutyy, cutx:cutxx].copy()
		
		gray	= cv2.cvtColor(small, cv2.COLOR_BGR2GRAY)
		
		#print self._mask.shape, gray.shape
		small_masked = cv2.bitwise_and(self._mask, gray)
		if self._last_img is None: self._last_img = small_masked

		diff = cv2.absdiff(small_masked, self._last_img)
		self._last_diff = diff.copy()
		self._last_diff[self._last_diff>self._threshold] = 255

		diff[diff<=self._threshold] = 0
		diff[diff>self._threshold]	= 1

		self._absmotion.append( np.sum(diff) )

		diff = np.float32(small_masked)-np.float32(self._last_img)
		self._motion.append( np.sum(diff) )
		self._last_img 	= small_masked
		
		


	def draw(self, index, frame, show_diff=True):
		if index>=len(self._positions): return
		x,y = self._positions[index]
		
		if show_diff:
			cv2.circle(frame, (x,y), self._radius, (0,0,0), -1)
			frame[y-self._radius:y+self._radius, x-self._radius:x+self._radius] += cv2.merge( (self._last_diff, self._last_diff, self._last_diff) )
		else:
			cv2.circle(frame, (x,y), self._radius, (0,0,255))

		font = cv2.FONT_HERSHEY_SIMPLEX
		cv2.putText(frame,self._name,(x-10,y-self._radius), font, 1,(0,255,255),2,cv2.LINE_AA)


	def export_2_csv(self, path, fly_index, led, total_n_flies=1, data2save=None):
		with open(os.path.join(path,'fly{0}_motion.csv'.format(self._name)), 'w') as csvfile:
			if data2save: csvfile.write( ';'.join(map(str,data2save))+'\n')

			self._events = sorted(self._events, key=lambda x: x)

			csvfile.write( ';'.join(['Frame','Motion','Abs motion','Velocity','Acceleration','Position X','Position Y','Led','Collided']+['Fly {0}'.format(i) for i in range(total_n_flies)])+'\n')
			for i, diff in enumerate(self._motion):
				pos 	= self._positions[i]
				vel 	= self._velocities[i]
				acc 	= self._accels[i]
				absdiff = self._absmotion[i]

				collisions = map(int, self._collisions[i])
				csvfile.write(';'.join(map(str,[i, diff, absdiff, vel, acc, pos[0], pos[1], led[i],int(sum(self._collisions[i])>0) ]+collisions))+'\n')


		with open(os.path.join(path,'fly{0}_events.csv'.format(self._name)), 'w') as csvfile:
			for i in range(fly_index):
				csvfile.write(';'.join(map(str,['T','Fly {0}'.format(i),'#6464ff']))+'\n')

			for i, (start,end,event) in enumerate(self._events):
				csvfile.write(';'.join(map(str,['P',False,start,end,'Collided with Fly {0}'.format(event),'#6464ff',fly_index]))+'\n')


	def check_collisions(self, frame_index, flies):
		events_indices = list(range(len(self._open_events)))
		events_2_create = []

		self._collisions.append([False for i in range(len(flies))])
					
		for fly_idx, fly in enumerate(flies):
			if fly._name!=self._name:
				if self.collide(frame_index, fly):
					self._collisions[-1][fly_idx] = True

					found = False
					for i in range(len(self._open_events)):
						event = self._open_events[i]
						if event[2]==fly._name:
							event[1] = frame_index
							events_indices.remove(i)
							found = True
							break

					if not found: events_2_create.append(fly_idx)

		for i in sorted(events_indices, reverse=True): del self._open_events[i]

		for fly_idx in events_2_create:
			event = [frame_index, frame_index, flies[fly_idx]._name]
			self._open_events.append(event)
			self._events.append(event)


	def collide(self, frame_index, fly):
		if frame_index>=len(self._positions): return False
		pos0 = self._positions[frame_index]
		pos1 = fly._positions[frame_index]
		dist = tools.lin_dist(pos0, pos1)
		return dist<self._radius


	@property
	def radius(self): return self._radius
	@radius.setter
	def radius(self, value): 
		self._radius = value
		self._mask 	 = np.zeros( (self._radius*2, self._radius*2), dtype=np.uint8 ); 
		cv2.circle(self._mask, (self._radius, self._radius), self._radius, 255, -1 )
		self.init()
	
	@property
	def threshold(self): return self._threshold
	@threshold.setter
	def threshold(self, value): self._threshold = value