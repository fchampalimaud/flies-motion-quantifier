import csv, cv2, os, numpy as np
from flies_motion_quantifier import tools
from flies_motion_quantifier.fly import Fly


SIZE = 30

class MotionCounter(object):

	def __init__(self):
		self._flies  = []
		self._radius = 30
		self._thresh = 5
		self.led	 = 50,50,50
		self._led_light = []
		

	def load_data(self, filename, no_flies):
		print('got in')
		self._led_mask 	= None
		self._led_light = []
		print('var done 1')
		self._flies 	= [Fly(x) for x in range(no_flies)]
		print('var done')
		infile = open(filename, 'rU')
		spamreader = csv.reader(infile, delimiter='\t', quotechar='|')
		next(spamreader)#.next() #skip the header
		print('csv done')
		for row in spamreader:
			for i in range(no_flies): self._flies[i].addPosition( row[i*3], row[i*3+1] )

	def process(self, frame):
		if self._led_mask==None:
			self._led_mask 	 = np.zeros( (self.led[2]*2, self.led[2]*2, 3), dtype=np.uint8 )
			cv2.circle(self._led_mask, (self.led[2], self.led[2]), self.led[2], (255,255,255), -1 )

			self._last_frame = frame[self.led[1]-self.led[2]:self.led[1]+self.led[2],self.led[0]-self.led[2]:self.led[0]+self.led[2]].copy()

			self._last_frame = cv2.bitwise_and(self._last_frame, self._led_mask)

		small = frame[self.led[1]-self.led[2]:self.led[1]+self.led[2],self.led[0]-self.led[2]:self.led[0]+self.led[2]].copy()
		small = cv2.bitwise_and(small, self._led_mask)

		diff = np.float32(small)-np.float32(self._last_frame)		
		self._led_light.append( np.sum(diff) )

		self._last_frame = small


	@property
	def flies(self): return self._flies

	@property
	def radius(self): return self._radius
	@radius.setter
	def radius(self, value): 
		self._radius = value
		for f in self._flies: f.radius = value

	@property
	def threshold(self): return self._thresh
	@threshold.setter
	def threshold(self, value): 
		self._thresh = value
		for f in self._flies: f.threshold = value

	@property
	def led(self): return self._led
	@led.setter
	def led(self, value): 
		self._led = value
			
	def export_2_csv(self, path):
		with open(os.path.join(path,'led.csv'), 'w') as csvfile:
			for i, diff in enumerate(self._led_light):
				csvfile.write(';'.join(map(str,[i,diff]))+'\n')
		


if __name__=='__main__':


	cap = cv2.VideoCapture('/home/ricardo/subversion/social-modulation-of-defence-behaviours-in-drosophila/data/Subjecta0/subjecta0.avi')
	motion = MotionCounter(); motion.load_data('/home/ricardo/subversion/social-modulation-of-defence-behaviours-in-drosophila/data/Subjecta0/trajectories_nogaps.txt', 5)

	while True:

		res, frame = cap.read()
		if not res: break

		index = int(cap.get(cv2.CAP_PROP_POS_FRAMES))

		for fly in motion.flies: fly.process(index, frame, SIZE)
		for fly in motion.flies: fly.check_collisions(index, motion.flies, SIZE)
		for fly in motion.flies: fly.draw(index, frame, SIZE)

		cv2.imshow('Frame', frame); key = cv2.waitKey(1);
		if ord('q')==key: break


	for i, fly in enumerate(motion.flies): 
		fly.export_2_csv( os.path.join('data','Subjecta0','output'), i )
		