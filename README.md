Flies motion quantifier
====================

The application quantifies the motion of flies in an arena.

##### Input

- **Video file**.
- **Flies positions file** - output file from the [idTracker](http://www.idtracker.es) or a file that respects the next format: 
	- X0	Y0	[empty column]	Xn	Yn	[empty column] ...
	- Each pair of "X0	Y0	[empty column]" values corresponds to the position of one fly.
	- There should be so many lines in the file as frames in the video. 
	- The spaces between the values in a line should be tabs ('\t').
- **No. flies** - Number of files coordenates present in the Flies positions file.
- **Led ROI** - The coordenates from a circumference surrounding the LED.
- **Threshold** - Define from which value the pixels intensity will be considered.
- **Radius** - Radius of the circle used to calculate the motion, around each fly.

##### Output

- The software generates a folder with the name "output" in the current running directory.
- The output folder contain the next files:
	- **fly[n]_events.csv**
		- This file is compatible with the [Python Video Annotator](https://github.com/UmSenhorQualquer/pythonVideoAnnotator) software, and describe the events when the fly collide with other flies.
	- **fly[n]_motion.csv**
		- This file contains the next information: frame; total intensity difference of the pixels, sum of pixels that changed after applying the threshold (if the change is lower than the threshold ignore the pixel), velocity, acceleration, x coordenate, y coordenate, collided, [Fly 1 collision, Fly n collision].
	- **led.csv** 
		- The file contains the led activation and deactivation information.



___
___

How it works
====================

##### Requirements
* Python 2.7 or 3.x
* Pyforms

##### Installation

Type the in the terminal the command: ``python setup.py install``

##### Run

Type in the terminal the command: ``flies-motion-quantifier``

## User manual

Select the video, the data file and press "Load".

![Load data](docs/imgs/load_data.png?raw=true "Screen")

Set the radius and the threshold values to calculate the motion.

Play the video to pre-visualize the configurations' result.


![Set the configurations](docs/imgs/set_threshold.png?raw=true "Screen")

In the application main menu, select the option "Algorithm >> Run", and wait until the progress bar gets to 100%.


![Run the analysis](docs/imgs/run.png?raw=true "Screen")

___

##### Analyse the results with the [Python Video Annotator](https://github.com/UmSenhorQualquer/pythonVideoAnnotator)

* Select the "Import" button in the timeline widget.
* After the import window appear, in the first combobox select the "Graph file" option.
* Select the fly[n]_motion.csv file and the "Value column" you want to import. The "Frame column" should remain with the value 0. Press the "Import" button.
* Repeat the file import for the "difference of pixels", "absolute difference of pixels" and velocity columns.
* [Optional] You can also load the led.csv file.

![Load data](docs/imgs/python_video_annotator_load_data.png?raw=true "Screen")

* In the application main menu select the options "Path >> Import" and import the fly[n]_motion.csv. 
* **Note:** do not forget to set the correct X and Y coordenates columns.

![Load the positions](docs/imgs/python_video_annotator_load_positions.png?raw=true "Screen")

- Select the "Import" button in the timeline widget.
- After the import window appear, and in the first combobox select the "Events file" option.
- Import the file fly[n]_events.txt to know the moments where the fly has collided with other flies.

![Load the events](docs/imgs/python_video_annotator_load_events.png?raw=true "Screen")

##### The final result

![Analysis](docs/imgs/python_video_annotator_final.png?raw=true "Screen")

___
___

Run the application on the terminal (just for version v1.x)
====================

1. Open the terminal.
2. Go to the application directory on the terminal.
3. Execute the command: **set PYFORMS\_MODE=TERMINAL**
4. Execute the command to view the paramenters: **flies-motion-quantifier\_v1.0.0\_git43\_build31\_DEV.exe --help**
5. Execute the command to run the application:

```bash
flies-motion-quantifier_v1.0.0_git43_build31_DEV.exe --_video_file "[VIDEO FILE]" --_no_flies 5 --_led_roi "[LED ROI]" --_threshold_slider [THRESHOLD VALUE] --_idtracker_file "[IDTRACKER FILE]" --_radius_slider [RADIUS VALUE] --exec execute
```

##### Example

```bash
cd c:\flies_motion_quantifier
set PYFORMS_MODE=TERMINAL
flies-motion-quantifier_v1.0.0_git43_build31_DEV.exe --_video_file "c:\video.avi" --_no_flies 5 --_led_roi "100,100,22" --_threshold_slider 5 --_idtracker_file "c:\tracking.txt" --_radius_slider 30 --exec execute
```