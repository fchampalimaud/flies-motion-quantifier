#!/usr/bin/python
# -*- coding: utf-8 -*-

with open('flies_motion_quantifier/__init__.py', 'r') as fd:
	__version__     = eval(fd.readline().split('=')[1])
	__author__      = eval(fd.readline().split('=')[1])
	__credits__     = eval(fd.readline().split('=')[1])
	__license__     = eval(fd.readline().split('=')[1])
	__maintainer__  = eval(fd.readline().split('=')[1])
	__email__       = eval(fd.readline().split('=')[1])
	__status__      = eval(fd.readline().split('=')[1])



from setuptools import setup

setup(
	name='Flies motion quantifier',
	version=__version__,
	description="""The application was created for the experiment: Social modulation of defence behaviours in Drosophila melanogaster.""",
	author='Ricardo Jorge Vieira Ribeiro',
	author_email='ricardojvr@gmail.com',
	license='MIT',
	
	packages=['flies_motion_quantifier'],
	install_requires=[
		"pyforms",        
	],
	entry_points={
		'console_scripts':['flies-motion-quantifier=flies_motion_quantifier.__main__']
	}
)
